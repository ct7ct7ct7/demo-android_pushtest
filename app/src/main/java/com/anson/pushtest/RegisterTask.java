package com.anson.pushtest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by Anson on 2015/8/22.
 */
public class RegisterTask extends AsyncTask<Void, Void, String> {
    private ProgressDialog mDialog;
    private Context mContext;
    private TextView mTokenTv;

    public RegisterTask(Context context,TextView tokenTv) {
        mDialog = new ProgressDialog(context);
        mDialog.setTitle("提示");
        mDialog.setMessage("註冊Token中~~~~");
        mDialog.setCancelable(false);

        mContext = context;
        mTokenTv = tokenTv;
    }

    @Override
    protected void onPreExecute() {
        mDialog.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        String token = null;
        try {
            token = GoogleCloudMessaging.getInstance(mContext).register(MainActivity.GCM_SENDER_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }


    @Override
    protected void onPostExecute(String token) {
        if (token == null) {
            //註冊token失敗
            Toast.makeText(mContext,"註冊失敗，請檢查你的 SENDER_ID 是否有設定正確",Toast.LENGTH_LONG).show();
        }else {
            mTokenTv.setText(token);
        }
        mDialog.cancel();
        super.onPostExecute(token);
    }
}