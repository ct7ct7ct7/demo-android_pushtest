package com.anson.pushtest;

import android.os.Bundle;
import com.google.android.gms.gcm.GcmListenerService;


/**
 * Created by Anson on 2015/8/22.
 */
public class GcmService extends GcmListenerService{
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString(MainActivity.TEST_MY_MESSAGE_KEY);
        System.out.println("收到推播了-from:" + from);
        System.out.println("收到推播了-data:" + message);
    }
}
