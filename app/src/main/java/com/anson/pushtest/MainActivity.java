package com.anson.pushtest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {
    public static final String GCM_SENDER_ID = "你的Sender ID";
    public static final String SERVER_API_KEY = "你的 Api Key";
    public static final String TEST_MY_MESSAGE_KEY = "MY_PUSH_MESSAGE";

    private TextView mTokenTv;
    private Button mRegisterBtn , mSendPushBtn;
    private EditText mMessageEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTokenTv = (TextView)findViewById(R.id.tokenTv);
        mRegisterBtn = (Button)findViewById(R.id.registerBtn);
        mSendPushBtn = (Button)findViewById(R.id.sendPushBtn);
        mMessageEt = (EditText)findViewById(R.id.messageEt);

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RegisterTask(MainActivity.this,mTokenTv).execute();
            }
        });
        mSendPushBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String targetToken = mTokenTv.getText().toString();
                String message = mMessageEt.getText().toString();
                new SendPushTask(MainActivity.this).execute(getPushMessage(targetToken,message));
            }
        });
    }

    /**
     * 取得推波訊息格式
     */
    private static String getPushMessage(String targetToken,String message) {
        String KEY_REGISTRATION_IDS = "registration_ids";
        String KEY_DATA = "data";

        JSONObject jObj = new JSONObject();
        JSONArray idsArray = new JSONArray();
        idsArray.put(targetToken);

        JSONObject jObjData = new JSONObject();
        try {
            jObjData.put(TEST_MY_MESSAGE_KEY,message);

            jObj.put(KEY_REGISTRATION_IDS, idsArray);
            jObj.put(KEY_DATA, jObjData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj.toString();
    }
}
