package com.anson.pushtest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Anson on 2015/8/22.
 */
public class SendPushTask extends AsyncTask<String, Void, Integer> {
    private ProgressDialog mDialog;
    private Context mContext;

    public SendPushTask(Context context) {
        mDialog = new ProgressDialog(context);
        mDialog.setTitle("提示");
        mDialog.setMessage("發送Push中~~");
        mDialog.setCancelable(false);
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        mDialog.show();
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(String... params) {
        int statusCode = -1;
        try {
            URL url = new URL("https://android.googleapis.com/gcm/send");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key = "+ MainActivity.SERVER_API_KEY);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(params[0]);
            writer.flush();
            writer.close();
            os.close();

            conn.connect();
            statusCode = conn.getResponseCode();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusCode;
    }


    @Override
    protected void onPostExecute(Integer statusCode) {
        if(statusCode!=200){
            Toast.makeText(mContext,"發送失敗，請檢查你的 SERVER_API_KEY 是否有設定正確",Toast.LENGTH_LONG).show();
        }
        mDialog.cancel();
        super.onPostExecute(statusCode);
    }
}